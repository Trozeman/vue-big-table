import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: true,
    save: true,
    position:{
      x:1,
      y:1
    },
    data: new Map()
  },
  mutations: {
    changePosition(state, {x,y}){
      state.position.x= x;
      state.position.y= y;
      return state
    },
    saveCellData(state, data){
      return state.data.set(data.key, data.value);
    },
    saveLoading(state, value){
      return state.loading = value;
    },
    buttonToggle(state, value){
      return state.save = value;
    }
  },
  actions: {
      SAVE_CHANGES(context, data) {
        context.commit("saveCellData", data);
      }
  }
})
