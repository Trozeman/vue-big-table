# grid-test-task

Click [here](https://trozeman.github.io/vue-big-table/) to see the build.

```
    It is the first time when I work with VUE CLI.
    
    It is a very interesting task and I tried to make a solution with the 
    best performance.
    
    I hope so I do not waste time... 
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
